What is it? null linux is a rebuild of [splitlinux](https://splitlinux.org) with modern features
*"Split Linux is a general operating system optimized for safely navigating hostile environments like the Internet and physical check points."*
![[Pasted image 20230714181622.png]]
Instead of logical volumes we use BTRFS
![[Pasted image 20230714182112.png]]
Instead of Xorg we will be using Wayland
as instead of DWM we will be using Hyprland

we will also be using musl only 

first we will wipe the nvme namespaces with 0xffffffff
```
nvme format -s1 /dev/nvme0n1
```
then create the UEFI boot partion:
```bash
parted /dev/nvme0n1 mklabel gpt
parted /dev/nvme0n1 mkpart ESP fat32 1MiB 513MiB name boot
parted /dev/nvme0n1 set 1 esp on
parted /dev/nvme0n1 set 1 boot on
mkfs.fat -F32 /dev/nvme0n1p1
```
This creates a 512 MiB EFI System Partition (ESP) named `boot` at the beginning of the NVMe drive and formats it with the FAT32 filesystem.
*Note: its best to keep boot on a seperate drive but for this alpha instructions we will be containing it on a separate partition*
next create a the luks partition: 
```bash
parted /dev/nvme0n1 mkpart primary 513MiB 100%
parted /dev/nvme0n1 name 2 root
```
This creates a partition for the filesystem named `root`
now lets encrypt it:
```bash
cryptsetup luksFormat --iter-time=5000 --hash=sha512 --key-size=512 --cipher=aes-xts-plain64 /dev/nvme0n1p2
cryptsetup open /dev/nvme0n1p2 luks
```
*Note: make sure to backup your luksheader to an encrypted usb drive aswell: *

*Note: I suggest a password with at least 16 characters, preferably more*

```bash
cryptsetup luksOpen /dev/sda4 usb 
mount /dev/mapper/usb /media/usb 
cryptsetup luksHeaderBackup /dev/nvme0n1p2 --header-backup-file /media/usb/luksheader.bak
sync
umount /media/usb
```
*note to self: add gpg encryption aswell and check if --allow-discards for trim is good on nvme0n1p1*

create the btrfs filesystem:
```bash
mkfs.btrfs /dev/mapper/luks
```
this creates a btrfs filesystem on the encrypted patrition

Mount the Btrfs filesystem and set up subvolumes:
```bash
mkdir /mnt/null
mount /dev/mapper/luks /mnt/null
btrfs subvolume create /mnt/null/@
btrfs subvolume create /mnt/null/@home
btrfs subvolume create /mnt/null/@snapshots
umount /mnt/null 
```
*Note: setup so /var/log and /var/log/audit are on seperate partitions aswell as /tmp and /var/tmp*

This creates three Btrfs subvolumes for the root filesystem, home directory, and snapshots.
```bash
mount -t btrfs -o noatime,relatime,compress=lzo,ssd,space_cache=v2,subvol=@ /dev/mapper/luks /mnt/null
```
*Note: we unmount /mnt/null so we can use the btrfs subvolume `@`*


```bash
cd /mnt/null
wget https://repo-default.voidlinux.org/live/current/void-x86_64-musl-ROOTFS-20230628.tar.xz
tar xvf void-<...>-ROOTFS.tar.xz -C /mnt/null
```
https://gist.github.com/gbrlsnchs/9c9dc55cd0beb26e141ee3ea59f26e21