``3# Void Linux Install




## Hardware Configuration
This configuration is for my Physical Server:
**HOST:** PowerEdge R710 
**CPU:** Intel Xeon L5640 (24) @ 2.262GHz (x2)
https://manualsdump.com/en/manuals/dell-idrac6/147859/193


2x 1000w power supplys 
gen1 motherboard


### Installation Media Prep
Download the ISO from the [downlod link](https://www.gentoo.org/downloads/)
and then to verify the ISO import the correct keys
```bash
gpg --keyserver hkps://keys.gentoo.org --recv-keys 0xBB572E0E2D182910
```
or if you are using a Gentoo verified install
```bash
gpg --import /usr/share/openpgp-keys/gentoo-release.asc
```
then verify the ISO
```bash
gpg --verify install-amd64-minimal-*.iso.asc
```
next dd the ISO to the target bootable drive:
```bash
dd if=/path/to/image.iso of=/dev/sdX bs=4MB conv=fsync oflag=direct status=progress
```


# Roadmap
- [ ] full disk encryption on root disk
- [ ] partitions
- [ ] btrfs
- [ ] zfs https://www.stephenwagner.com/2020/06/06/freenas-truenas-zfs-optimizations-considerations-ssd-nvme/
	maybe we can have nvme/ssd caching with a 14tb parity/backup drive or 2 of them
	https://arstechnica.com/information-technology/2020/05/zfs-101-understanding-zfs-storage-and-performance/
	https://superuser.com/questions/849235/beginner-backing-up-zfs-storage-pools#849300 
- [ ] selinux or app armor
- [ ] lxd on zfs with networking outside and inside of network
- [ ] firehol and iptable rules for putting it head of network

## stack:
https://github.com/navilg/media-stack
Jellyfin, Radarr, Sonnar, Prowlerr or jackett + qbittorrent (over netherlands server for torrenting vpn) jellyfin on lan or over dns (zoa.sh or femboy.zip)
nextcloud (zoa.sh)
znc or another bouncer preferably something nice in rust (tcp.wiki or malware.social or femboy.zip) https://sr.ht/~emersion/soju/ + https://git.sr.ht/~emersion/gamja
matrix probably synapse in rust (malware.social and femboy.zip) https://conduit.rs/
vault warden for protecting me and clients logins (set 2fa to required) https://github.com/dani-garcia/vaultwarden 
gitea (with custom epic theme, mirror to github) https://github.com/go-gitea/gitea
(everything should be themed pretty epicly)
invoice ninja (invoice.cursed.tech)

srcs:
[Pratical Hardening Guide](https://github.com/trimstray/the-practical-linux-hardening-guide)
[Hardening Checklist](https://github.com/trimstray/linux-hardening-checklist)
[DevSec Hardening](https://dev-sec.io/) and [DevSec Github](https://github.com/dev-sec/)

data scrubbing: https://kb.synology.com/en-global/DSM/help/DSM/StorageManager/storage_pool_data_scrubbing?version=7