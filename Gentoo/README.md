# Gentoo Linux Install
Goal is to be a super secure modern linux install for daily use
we will doing a super big brain install using [distcc](https://wikitest.gentoo.org/wiki/Distcc/en) (for faster compiling)


UEFI + GPT (for secure boot using grub2)
LUKS + btrfs 

needs to mute and hopefully pause any media on closing and and turned off and muted with webcam on boot



## Hardware Configuration
This configuration is for my laptop:
**HOST:** 20Y70096US ThinkPad E14 Gen 3
**CPU:** AMD Ryzen 7 5700U (16) @ 1.800GHz
https://wiki.archlinux.org/title/Lenovo_ThinkPad_E14_Gen_3_(AMD)

https://github.com/ramaureirac/thinkpad-e14-linux



## Pre Installation
### Installation Media Prep
Download the ISO from the [downlod link](https://www.gentoo.org/downloads/)
and then to verify the ISO import the correct key [signatures](https://www.gentoo.org/downloads/signatures/)
```bash
gpg --keyserver hkps://keys.gentoo.org --recv-keys 0xBB572E0E2D182910
```
or if you are using a Gentoo verified install
```bash
gpg --import /usr/share/openpgp-keys/gentoo-release.asc
```
then verify the ISO
```bash
gpg --verify install-amd64-minimal-*.iso.asc
```
next dd the ISO to the target live bootable device:
```bash
dd if=/path/to/image.iso of=/dev/sdX bs=4MB conv=fsync oflag=direct status=progress
```
*Note: replace `/dev/sdX` with your target live boot device*

Now go ahead and boot into the Live USB

### Configure Network:
If you're connected via ethernet then the dhcp server should have assigned you a IP 
```bash
ip -c a 
```
if you're connecting via wireless then you can setup with wpa_supplication

you also may want to enable ssh if you want to access from another computer
```bash
rc-service sshd start
ip -c a
```
### Partitioning
find your target drive using `lsblk`
lets use nvme1n1 as the drive we are installing on
- [ ] prep drives by random formating? this could be bad for the nvme make sure to set warnings on off boot types 
| Partition | Filesystem | Size | Label |
|-----------|------------|------|-------------|
| /dev/nvme1n1p1 | fat32 | 512Mib | boot |
| /dev/nvme1n1p2 | LUKS  | ALL | root |

create the UEFI boot partion:
```bash
parted /dev/nvme1n1 mklabel gpt
parted /dev/nvme1n1 mkpart ESP fat32 1MiB 513MiB name boot
parted /dev/nvme1n1 set 1 esp on
parted /dev/nvme1n1 set 1 boot on
mkfs.fat -F32 /dev/nvme1n1p1
```
This creates a 512 MiB EFI System Partition (ESP) named `boot` at the beginning of the NVMe drive and formats it with the FAT32 filesystem.

next create a the luks partition: 
```bash
parted /dev/nvme1n1 mkpart primary 513MiB 100%
parted /dev/nvme1n1 name 2 root
```
This creates a partition for the filesystem named `root`
now lets encrypt it:
```bash
cryptsetup luksFormat --iter-time=5000 --hash=sha512 --key-size=512 --cipher=aes-xts-plain64 /dev/nvme1n1p2
cryptsetup open /dev/nvme1n1p2 luks
```
*Note: make sure to backup your luksheader to an encrypted usb drive aswell: *
```bash
cryptsetup luksOpen /dev/sda4 usb 
mount /dev/mapper/usb /media/usb 
cryptsetup luksHeaderBackup /dev/nvme1n1p2 --header-backup-file /media/usb/luksheader.bak
sync
umount /media/usb
```
*note to self: add gpg encryption aswell and check if --allow-discards for trim is good on nvme1n1p1* [here](https://wiki.gentoo.org/wiki/Dm-crypt_full_disk_encryption) 

create the btrfs filesystem:
```bash
mkfs.btrfs /dev/mapper/luks
```
this creates a btrfs filesystem on the encrypted patrition

Mount the Btrfs filesystem and set up subvolumes:
```bash
mount /dev/mapper/luks /mnt/gentoo
btrfs subvolume create /mnt/gentoo/@
btrfs subvolume create /mnt/gentoo/@home
btrfs subvolume create /mnt/gentoo/@snapshots
umount /mnt/gentoo 
```
*Note: setup so /var/log and /var/log/audit are on seperate partitions aswell as /tmp and /var/tmp*

This creates three Btrfs subvolumes for the root filesystem, home directory, and snapshots.
```bash
mount -t btrfs -o noatime,relatime,compress=lzo,ssd,space_cache=v2,subvol=@ /dev/mapper/luksdev /mnt/gen
```
*Note: we unmount /mnt/gentoo so we can use the btrfs subvolume `@`*

# pre Installation
```bash
cd /mnt/gentoo/
```
check the date and setup ntpd
now we must wget our stage3
```bash
wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20230416T164657Z/stage3-amd64-hardened-openrc-20230416T164657Z.tar.xz
```
*Note: Rewrite this to auto pull newest one*
```bash
sha512sum stage3-amd64-*.tar.xz
gpg --verify stage3-amd64-*.tar.xz{.DIGESTS.asc,}
```
*The fingerprints of the OpenPGP keys used for signing release media can be found on the [release media signatures page](https://www.gentoo.org/downloads/signatures/) of the Gentoo webserver.*
we verify the signature then we can unpack it
```bash
tar xvpf stage3-amd64-*.tar.xz --xattrs-include='*.*' --numeric-owner
```
now we must open /mnt/gentoo/etc/portage/make.conf and configure with our optimization and use variables
```bash
# These settings were set by the catalyst build script that automatically
# built this stage.
# Please consult /usr/share/portage/config/make.conf.example for a more
# detailed example.
COMMON_FLAGS="-march=native -O2 -pipe"

MAKEOPTS="-j8 -l8"

# DISTCC
#COMMON_FLAGS="-march=znver2 -O2 -pipe"
# 1 remote host with two cpus 6 cores each = 12 cores remote
# 1 local host with 8 cores = 8 cores local
# total number of cores is 20, so N = 2*20+1 and M=8
#MAKEOPTS="-j41 -l8"
#FEATURES="distcc distcc-pump"

CFLAGS="${COMMON_FLAGS}"
CXXFLAGS="${COMMON_FLAGS}"
FCFLAGS="${COMMON_FLAGS}"
FFLAGS="${COMMON_FLAGS}"

# NOTE: This stage was built with the bindist Use flag enabled
CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt rdrand sha sse sse2 sse3 sse4_1 sse4_2 sse4a ssse3"

ACCEPT_KEYWORDS="~amd64"
ACCEPT_LICENSE="* -@EULA"

EMERGE_DEFAULT_OPTS="--autounmask=n"

VIDEO_CARDS="amdgpu radeonsi"
INPUT_DEVICES="libinput"

USE="widgets screen tcl qml png jpg gtk3 spice jpeg icu gles2 haptic gtk dbus minizip -bindset x264 fontconfig truetype egl wayland x11-backend X threads gtk2 -qt4 -nvidia -gnome -kde -dvd -systemd elogind alsa amdgpu vulkan mesa radeon libinput postproc xinerama pulseaudio cups text abi_x86_32 script offensive"
# REORGANIZE ALL THIS AND SETUP PACKAGE.USE FLAGS
USE="-systemd offensive \
	cryptsetup hardened elogind\
	wayland xwayland X -kde -gnome -qt5 -gtk \
	alsa amdgpu vulkan lm-sensors\ 
	-nvidia -dvd \
"

#QEMU_SOFTMMU_TARGETS="arm x86_64 sparc"
#QEMU_USER_TARGETS="x86_64"
#RUBY_TARGETS="ruby31"

# NOTE: This stage was built with the bindist Use flag enabled
PORTDIR="/var/db/repos/gentoo"
DISTDIR="/var/cache/distfiles"
PKGDIR="/var/cache/binpkgs"

# This sets the language of build output to English.
# Please keep this setting intact when reporting bugs.
LC_MESSAGES=C

GRUB_PLATFORMS="efi-64"
```
*Note: check each and every one of these for very specific hardened setup* https://www.youtube.com/watch?v=NELUsKUn-1U&t=66s 
*Note: setup for distcc*

https://wiki.gentoo.org/wiki/Ryzen#Kernel (amd ryzen 7)
https://wiki.gentoo.org/wiki/AMDGPU (amdgpu radeonsi) https://wiki.gentoo.org/wiki/ATI_FAQ
https://wiki.gentoo.org/wiki/Vulkan (vulkan)
https://wiki.gentoo.org/wiki/Wayland (wayland)
https://wiki.gentoo.org/wiki/User:Sakaki/Sakaki%27s_EFI_Install_Guide/Setting_up_the_GNOME_3_Desktop_under_OpenRC (wayland2)
https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Features
check out userfetch
https://amedeos.github.io/gentoo/2021/04/25/Unlock-rootfs-with-fido2-key.html setup fido2 security
https://amedeos.github.io/backup/2021/08/18/Use-btrbk-for-backup-on-btrfs.html setup remote backups
https://amedeos.github.io/gentoo/2019/01/17/gentoo-signed-kernel-module.html setup signed kernels
https://github.com/d333rboy/portage_presets/blob/main/etc/portage/make.conf (hardened config)
https://www.youtube.com/watch?v=NQStgCyezz4&t=13s (hardened install)

copy dns info
```bash
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
```


```bash
mount -t proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
test -L /dev/shm && rm /dev/shm && mkdir /dev/shm
mount -t tmpfs -o nosuid,nodev,noexec shm /dev/shm
chmod 1777 /dev/shm
```
*Note: check warnings https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Base*

now chroot
```bash
chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chroot) ${PS1}"
```
# Installation

https://www.youtube.com/watch?v=J7W9MItUSGw (comfy install guide)
syncronise repositories
```bash
emerge --sync

emerge --oneshot portage
```

now update world and set new use
```bash
eselect profile list
eselect profile set 3
emerge --ask --verbose --update --deep --newuse @world
```
write the timezone and locals if needed
```bash 
echo "America/Denver" > /etc/timezone
emerge --config sys-libs/timezone-data
nano -w /etc/locale.gen
locale-gen
eselect locale list
eselect locale set 3
env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
```

now to emerge kernel sources and linux firmware
```bash
emerge -av sys-kernel/linux-firmware
emerge --ask sys-kernel/gentoo-sources
eselect kernel list
eselect kernel set 1
```
## Kernel Compiling and Initramfs
we can do this by installing pciutils:
```bash
emerge --ask sys-apps/pciutils
```
then run `lspci`
```bash
lspci
```
the output should look something like this:
```
00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne Root Complex
00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne IOMMU
00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge
00:02.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge
00:02.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge
00:02.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge
00:02.3 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge
00:02.4 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne PCIe GPP Bridge
00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir PCIe Dummy Host Bridge
00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Renoir Internal PCIe GPP Bridge to Bus
00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 51)
00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 0
00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 1
00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 2
00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 3
00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 4
00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 5
00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 6
00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Renoir Device 24: Function 7
01:00.0 Non-Volatile memory controller: Phison Electronics Corporation PS5013 E13 NVMe Controller (rev 01)
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 10)
03:00.0 Network controller: Intel Corporation Wi-Fi 6 AX200 (rev 1a)
04:00.0 Non-Volatile memory controller: SK hynix Gold P31/PC711 NVMe Solid State Drive
05:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Lucienne (rev c1)
05:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Renoir Radeon High Definition Audio Controller
05:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
05:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne USB 3.1
05:00.4 USB controller: Advanced Micro Devices, Inc. [AMD] Renoir/Cezanne USB 3.1
05:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] ACP/ACP3X/ACP6x Audio Coprocessor (rev 01)
05:00.6 Audio device: Advanced Micro Devices, Inc. [AMD] Family 17h/19h HD Audio Controller
```
Then we should start building the kernel:
```bash
cd /usr/src/linux
make menuconfig
```
Lets first enable OpenRC since we are using that
```
Gentoo Linux --->
  Generic Driver Options --->
    [*] Gentoo Linux support
    [*]   Linux dynamic and persistent device naming (userspace devfs) support
    [*]   Select options required by Portage features
        Support for init systems, system and service managers  --->
          [*] OpenRC, runit and other script based systems and managers
```
Next we should add NVME support:
```
Device Drivers --->
  NVME Support --->
    <*> NVM Express block device
    [*] NVMe multipath support
	[*] NVMe hardware monitoring
	<M> NVM Express over Fabrics FC host driver
	<M> NVM Express over Fabrics TCP host driver
	<M> NVMe Target support
	  [*]   NVMe Target Passthrough support
	  <M>   NVMe loopback device support
	  <M>   NVMe over Fabrics FC target driver
	  < >     NVMe over Fabrics FC Transport Loopback Test driver (NEW)
	  <M>   NVMe over Fabrics TCP target support
```
We will also need Filesystem support:
```
File systems --->
  <*> Second extended fs support
  <*> The Extended 3 (ext3) filesystem
  <*> The Extended 4 (ext4) filesystem
  <*> Btrfs filesystem support
  DOS/FAT/NT Filesystems  --->
    <*> MSDOS fs support
    <*> VFAT (Windows-95) fs support
  Pseudo Filesystems --->
    [*] /proc file system support
    [*] Tmpfs virtual memory file system support (former shm fs)
```
We also need to add support for our multicore system:
```
Processor type and features  --->
  [*] Symmetric multi-processing support
```
Aswell as USB devices:
```
Device Drivers --->
  HID support  --->
    -*- HID bus support
    <*>   Generic HID driver
    [*]   Battery level reporting for HID devices
      USB HID support  --->
        <*> USB HID transport layer
  [*] USB support  --->
    <*>     xHCI HCD (USB 3.0) support
    <*>     EHCI HCD (USB 2.0) support
    <*>     OHCI HCD (USB 1.1) support
  <*> Unified support for USB4 and Thunderbolt  --->
```
Now we need to add GPT support:
```
-*- Enable the block layer --->
   Partition Types --->
      [*] Advanced partition selection
      [*] EFI GUID Partition support
```
aswell as UEFI:
```
Processor type and features  --->
    [*] EFI runtime service support 
    [*]   EFI stub support
    [*]     EFI mixed-mode support
 
Device Drivers
    Firmware Drivers  --->
        EFI (Extensible Firmware Interface) Support  --->
            <*> EFI Variable Support via sysfs
    Graphics support  --->
        Frame buffer Devices  --->
            <*> Support for frame buffer devices  --->
                [*]   EFI-based Framebuffer Support
```
*Note: this all comes from the [gentoo guide](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Kernel) so we should double check everything else*
we also need to add initramfs support:
```
General setup  --->
   [*] Initial RAM filesystem and RAM disk (initramfs/initrd) support
Device Drivers --->
   Generic Driver Options --->
      [*] Maintain a devtmpfs filesystem to mount at /dev
```

we also need to enable crypt support:
```kenel **Enabling device mapper and crypt target**
[*] Enable loadable module support
    Device Drivers --->
        [*] Multiple devices driver support (RAID and LVM) --->
            <*> Device mapper support
            <*> Crypt target support
```

```kernel **Enabling cryptographic API functions for the cipher you used**
[*] Cryptographic API --->
    <*> SHA224 and SHA256 digest algorithm
    <*> XTS support
    <*> AES cipher algorithms
    <*> AES cipher algorithms (x86_64)
```



now we must build the kernel:
```bash
make -j9 && make modules_install && make install
```
https://wiki.gentoo.org/wiki/Full_Encrypted_Btrfs/Native_System_Root_Guide
setup initramfs - [dracut](https://wiki.archlinux.org/title/Dracut) [readme](https://aj.immo/2021/11/gentoo-with-efistub-encrypted-btrfs/) [readme2](https://wstrm.dev/posts/gentoo_with_dm-crypt_luks/)
```bash
emerge --ask sys-kernel/dracut
```
*Note: https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Kernel#Optional:_Building_an_initramfs
https://wiki.gentoo.org/wiki/Kernel/Optimization
https://wiki.gentoo.org/wiki/Initramfs
https://forums.gentoo.org/viewtopic-t-1146237-view-previous.html
https://github.com/fraxgut/guia-instalacion-gentoo
https://gist.github.com/renich/90e0a5bed8c7c0de40d40ac9ccac6dfd*
we will now generate the initramfs:
```bash
dracut --hostonly --kver=<kernelversionandnamehere> 
```
*Note we need to double check for [dracut](https://wiki.gentoo.org/wiki/Dracut)*
we also should modify **`/etc/dracut.conf`** for the correct modules
```bash
# Equivalent to -H
hostonly="yes"

# Equivalent to -m "module module module"
dracutmodules+=" bash kernel-modules rootfs-block udev-rules usrmount base fs-lib shutdown "

# Equivalent to -a "module"
add_dracutmodules+=" module " # Note leading and trailing spaces

# Equivalent to -o "module"
omit_dracutmodules+=" module " # Note leading and trailing spaces

# Equivalent to --drivers="module module module"
drivers+=" module module module "

# Equivalent to --add-drivers
add_drivers+=" module "

# Equivalent to --omit-drivers="module"
omit_drivers+=" module "

# Equivalent to --filesystems="fs fs fs"
filesystems+=" fs fs fs "

# Equivalent to --kmoddir="/lib/modules/fixed"
drivers_dir="/lib/modules/fixed"

# Equivalent to  --fwdir=":/lib/fw/alt:/lib/fw/alt2"
fw_dir+=":/lib/fw/alt:/lib/fw/alt2"
```
- [ ] todo
	- [ ] checkout crypt-gpt
	- [ ] crypt
	- [ ] uefi-lib
	- [ ] 
## fstab

# Roadmap
https://github.com/dogoncouch/gentoo-laptop
https://github.com/kabiconfigs/Gentoo-Laptop-Config/blob/main/portage/make.conf
https://youtu.be/tkPDNLfmQZY
https://www.youtube.com/watch?v=oI_rpiqCHpU
https://www.youtube.com/watch?v=nOcS6mwZEGI (kernel)
- [ ] full disk encryption on root disk
- [ ] musl? 
	- [ ] https://www.youtube.com/watch?v=T-PTboW-GbQ gaming
	- [ ] **Musl stages: consider the musl overlay**While the core system packages within the stage files work fine in the main gentoo repository, musl-specific fixes for additional packages can be found in the [musl overlay](https://gitweb.gentoo.org/proj/musl.git/). If you encounter problems, you may want to consider adding this overlay to your installation. Instructions how to do so can be found, e.g., on the [page of the Hardened musl project](https://wiki.gentoo.org/wiki/Project:Hardened_musl). Please file bugs!
	- [ ] https://wiki.gentoo.org/wiki/Project:Musl
	- [ ] 
- [ ] partitioning 
- [ ] encrypting btrfs https://wiki.archlinux.org/title/Btrfs add compression?
- [ ] swapfile encrypted
- [ ] uefi
- [ ] distcc - https://wiki.gentoo.org/wiki/Distcc#To_bootstrap
- [ ] initramfs!!! 
- [ ] tpm secure boot?
- [ ] yubikey pam 
- [ ] app armor?
- [ ] ftrim + trim https://wiki.archlinux.org/title/Solid_state_drive#TRIM https://bbs.archlinux.org/viewtopic.php?id=239160 
- [ ] make use flags!!! https://www.youtube.com/watch?v=BkpvZYDMX34
- [ ] distcc setup (https://wiki.gentoo.org/wiki/Distcc)
- [ ] git setup 
- [ ] zshrc +starship
- [ ] hardened firefox config
- [ ] hardened chromium config
- [ ] tor and i2p configuration
- [ ] wireguard security and vpn security and firehol
- [ ] proxychains
- [ ] weechat security
- [ ] power management
- [ ] nix configuration 
- [ ] qemu with immo boot option
- [ ] ecryptfs on login https://wiki.archlinux.org/title/Data-at-rest_encryption#Block_device_vs_stacked_filesystem_encryption https://wiki.archlinux.org/title/ECryptfs https://sysphere.org/~anrxc/j/articles/ecryptfs/index.html https://wiki.gentoo.org/wiki/ECryptfs (supports tpm and gpg)
- [ ] https://wiki.gentoo.org/wiki/GLSA - security advisory 
- [ ] 
dispatch-conf (https://wiki.gentoo.org/wiki/Dispatch-conf) https://www.youtube.com/watch?v=en-aLJGXEqI
https://www.youtube.com/watch?v=nZLGvz9bMmc custom ebuild repository
tpm - https://wiki.gentoo.org/wiki/Trusted_Platform_Module
https://www.youtube.com/watch?v=jhfiDcrA8ZQ (post install)
https://resources.infosecinstitute.com/topic/gentoo-hardening-part-1-introduction-hardened-profile-2/

https://wiki.archlinux.org/title/OpenRC
https://old.calculate-linux.org/main/en/openrc_manuals
https://wiki.gentoo.org/wiki/OpenRC


!!! https://wiki.alpinelinux.org/wiki/Setting_up_a_laptop

with mutar!!! gpu pass through https://www.youtube.com/watch?v=YUU7ahzDyOg IMMOU single gpu https://github.com/joeknock90/Single-GPU-Passthrough

### ricing - https://www.youtube.com/watch?v=dFkGNe4oaKk
*Note: use stow for managing dotfiles*
https://arewewaylandyet.com/
https://github.com/hyprland-community/awesome-hyprland
- [ ] wayland & [hyprland](https://github.com/hyprwm/Hyprland) also [seatd????](https://wiki.gentoo.org/wiki/Seatd) https://github.com/JaKooLit/Ja_HyprLanD-dots https://github.com/slchris/hyprland-dotfiles
- [ ] [kitty](https://github.com/kovidgoyal/kitty) gpu based terminal 
- [ ] [helix](https://github.com/helix-editor/helix) vim alternative in rust
- [ ] https://github.com/sayanarijit/xplr file manager
- [ ] waybar - menu status bar
- [ ] [swww](https://github.com/Horus645/swww) for wallpaper
- [ ] [starship](https://github.com/starship/starship) shell prompt alternative to oh-my-zsh or oh-my-bash in rust for zsh or bash 
- [ ] xdg base directory specific - https://github.com/b3nj5m1n/xdg-ninja https://wiki.archlinux.org/title/XDG_Base_Directory https://github.com/qarmin/czkawka
- [ ] [zoxide](https://github.com/ajeetdsouza/zoxide) smarter cd alternative in rust
- [ ] alternative coreutils in rust https://github.com/uutils/coreutils
- [ ] https://github.com/orhun/systeroid alternative syctl more powerful and in rust
- [ ] ripgrep instead of find and grep or atomic grep???
- [ ] setup function keys
- [ ] https://github.com/sudofox/shell-mommy 


srcs:
https://wiki.archlinux.org/title/Laptop#top-page
[Pratical Hardening Guide](https://github.com/trimstray/the-practical-linux-hardening-guide)
[Hardening Checklist](https://github.com/trimstray/linux-hardening-checklist)
https://wiki.gentoo.org/wiki/Power_management/Guide 
https://linuxsecurity.expert/checklists/linux-security-and-system-hardening#hardware-security
setup yubikey pam: https://wiki.gentoo.org/wiki/YubiKey#plugdev
yubikey 2fa disk encryption: https://blog.mimacom.com/fde-with-yubikey/
yubikey fulldisk encryption with gpg: https://wiki.archlinux.org/title/YubiKey#Full_disk_encryption_with_LUKS
quick shutdown 
gpg keyfile on usb https://wiki.gentoo.org/wiki/Dm-crypt_full_disk_encryption
https://github.com/safing/portmaster - Block Mass Surveillance
luksNUKE: https://askubuntu.com/questions/821881/luks-and-nuke-key-installtion-on-ubuntu https://salsa.debian.org/pkg-security-team/cryptsetup-nuke-password

https://wiki.gentoo.org/wiki/Full_Disk_Encryption_From_Scratch_Simplified
https://www.linuxcapable.com/install-clamav-on-fedora-linux/ (clamav)
https://wiki.gentoo.org/wiki/ClamAV https://wiki.archlinux.org/title/ClamAV
tpm https://wiki.gentoo.org/wiki/Trusted_Platform_Module
secureboot https://wiki.gentoo.org/wiki/User:Sakaki/Sakaki%27s_EFI_Install_Guide/Configuring_Secure_Boot_under_OpenRC


https://amedeos.github.io/gentoo/2020/12/26/install-gentoo-with-uefi-luks-btrfs-and-systemd.html

https://www.reddit.com/r/Gentoo/comments/10e4a4l/installation_with_fulldisk_twofactor_encryption/

https://github.com/rememberYou/dotfiles/wiki/Installation
https://wiki.gentoo.org/wiki/Btrfs/Encrypted_Btrfs_System_Root_Guide
https://wiki.gentoo.org/wiki/User:Sakaki/Sakaki%27s_EFI_Install_Guide
https://wiki.gentoo.org/wiki/Dm-crypt_full_disk_encryption
https://wiki.gentoo.org/wiki/User:Sakaki/Sakaki%27s_EFI_Install_Guide


https://www.youtube.com/watch?v=I3uIl9w287g

                │15:14:21          @mss | https://git.adelielinux.org/adelie/gcompat                                                                                                                                             │+garyOak
                │15:14:21          @mss | https://github.com/ericonr/argp-standalone                                                                                                                                             │+jiggawatt
                │15:14:21          @mss | https://github.com/kaniini/libucontext                                                                                                                                                 │+kaizoku
                │15:14:21          @mss | https://github.com/pikhq/musl-nscd                                                                                                                                                     │+kakama
                │15:14:21          @mss | https://github.com/void-linux/musl-fts                                                                                                                                                 │+kayos
                │15:14:21          @mss | https://github.com/void-linux/musl-obstack                                                                                                                                             │+liszt_
                │15:14:21          @mss | https://skarnet.org/software/utmps/